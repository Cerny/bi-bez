// cernym46
// g++ -lssl -lcrypto lab6.cpp; ./a.out file cert.cer certificate.cer
// openssl x509 -text -in cert

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <unistd.h>
#include <iostream>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <openssl/evp.h>
#include <openssl/ssl.h>
#include <openssl/pem.h>

using namespace std;

#define HTTP_REQUEST "GET /student/odkazy HTTP/1.1\r\nConnection: close\r\nHost: fit.cvut.cz\r\n\r\n"
#define IP_ADDRESS "147.32.232.248" // nslookup fit.cvut.cz
#define BUFFER_SIZE 4096
#define PORT 443

void downloadToFile(char buffer[BUFFER_SIZE], SSL *ssl, char *outputFilePath) {
    snprintf(buffer, BUFFER_SIZE, HTTP_REQUEST);
    if (SSL_write(ssl, buffer, strlen(buffer) + 1) <= 0) {
        cout << "Error: SSL_write" << endl;
        exit(1);
    }
    int res;
    FILE *output_file = fopen(outputFilePath, "w");
    while ((res = SSL_read(ssl, buffer, BUFFER_SIZE)) > 0) {
        fwrite(buffer, sizeof(char), res, output_file);
    }
    fprintf(output_file, "\n");
    fclose(output_file);
    cout << "File was saved to " << outputFilePath << endl;
}

void saveCertificateToFile(SSL *ssl, char *outputFilePath) {
    X509 *cert = SSL_get_peer_certificate(ssl);
    if (!cert) {
        cout << "Error: SSL_get_peer_certificate" << endl;
        exit(1);
    }
    FILE *cert_file = fopen(outputFilePath, "w");
    if (!PEM_write_X509(cert_file, cert)) {
        cout << "Error: PEM_write_X509" << endl;
        exit(1);
    }
    fclose(cert_file);
    cout << "Certificate was saved to " << outputFilePath << endl;

    
}

void printInfoAboutCertificate(char *outputFilePath) {
    string command = "openssl x509 -text -in " + string(outputFilePath);
    system(command.c_str());
}

void getCurrentCipher(SSL *ssl) {
    const SSL_CIPHER *cipher = SSL_get_current_cipher(ssl);
    cout << "Used cipher: " << SSL_CIPHER_get_name(cipher) << endl;

    // Output: ECDHE-RSA-AES256-GCM-SHA384
    // ECDHE: Elliptic-curve Diffie–Hellman (ECDH) is an anonymous key agreement protocol that allows two parties, each having an elliptic-curve public–private key pair, to establish a shared secret over an insecure channel.
    // RSA: is one of the first public-key cryptosystems and is widely used for secure data transmission.
    // AES256: Advanced Encryption Standard
    // GCM: Galois/Counter Mode (GCM) is a mode of operation for symmetric-key cryptographic block ciphers
    // SHA256: hash algorithm
}

void createConnection(int &sockfd) {
    struct sockaddr_in servaddr;

    sockfd=socket(AF_INET,SOCK_STREAM,0);
    
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr(IP_ADDRESS); //ip address
    servaddr.sin_port = htons(PORT); // port
    if (connect(sockfd, (struct sockaddr *) &servaddr, sizeof(servaddr)) != 0) {
        cout << "Error: connect" << endl;
        exit(1);
    }
}

void printListOfAvailableCiphers(SSL *ssl) {
    int priority = 0;
    const char *item;
    cout << "Available ciphers:\n";
    while ((item = SSL_get_cipher_list(ssl, priority++)))
        cout << item << ", ";
    cout << endl;
}

void disableCipher(SSL *ssl) {
    SSL_set_cipher_list(ssl, "ALL:!ECDHE-RSA-AES256-GCM-SHA384");
    // It's changed to ECDHE-RSA-AES128-GCM-SHA256
}

int main(int argc, char *argv[]) {
    // Check number of arguments
    if (argc != 4) {
        cout << "Usage: " << argv[0] << " output_file output_cert_file rootCertificates" << endl;
        exit(1);
    }

    // Init
    SSL_library_init();
    char buffer[BUFFER_SIZE];

    int sockfd;
    createConnection(sockfd);

    // Create context
    SSL_CTX *ctx = SSL_CTX_new(SSLv23_client_method());
    if (!ctx) {
        cout << "Error: SSL_CTX_new" << endl;
        exit(1);
    }
    SSL_CTX_set_options(ctx, SSL_OP_NO_SSLv2 | SSL_OP_NO_SSLv3 | SSL_OP_NO_TLSv1);

    if (!SSL_CTX_load_verify_locations(ctx, argv[3], NULL)) {
        cout << "Error: SSL_CTX_load_verify_locations" << endl;
        exit(1);
    }
    if (!SSL_CTX_set_default_verify_paths(ctx)) {
        cout << "Error: SSL_CTX_set_default_verify_paths" << endl;
        exit(1);
    } 

    // Create SSL structure
    SSL *ssl = SSL_new(ctx);
    if (!ssl) {
        cout << "Error: SSL_new" << endl;
        exit(1);
    }
    if (!SSL_set_fd(ssl, sockfd)) {
        cout << "Error: SSL_set_fd" << endl;
        exit(1);
    }

    disableCipher(ssl);
    
    if (SSL_connect(ssl) <= 0) {
        cout << "Error: SSL_connect" << endl;
        exit(1);
    }

    getCurrentCipher(ssl);

    if (SSL_get_verify_result(ssl) == 0) {
        cout << "Certificate is not valid\n";
    } else {
        cout << "Certificate is valid\n";
    }
    
    downloadToFile(buffer, ssl, argv[1]);
    saveCertificateToFile(ssl, argv[2]);
    // printInfoAboutCertificate(argv[2]);
    printListOfAvailableCiphers(ssl);

    // Clean
    SSL_shutdown(ssl);
    close(sockfd);
    SSL_free(ssl);
    SSL_CTX_free(ctx);
    return 0;
}