#/bin/bash

if [ $# -lt 1 ]; then 
    echo "Error - number of parameters, example ./testLab3 ecb/cbc path"
    exit
fi

for f in $2/*; do
    ./a.out -e "$1" "$f"
done

for f in $2/*; do
    suffix=${f: -8}
    if [ "$suffix" = "_$1.bmp" ]; then        
        ./a.out -d "$1" "$f"
    fi
done

for f in $2/*; do
    suffix=${f: -8}
    if [ "$suffix" = "_dec.bmp" ]; then
        origFile="$(echo "$f" | rev | cut -c13- | rev).bmp"
        diff "$origFile" "$f"
    fi
done
