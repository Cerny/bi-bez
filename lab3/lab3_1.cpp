// Michal Cerny

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <openssl/evp.h>

#define enc 1
#define dec 0

unsigned char key[EVP_MAX_KEY_LENGTH] = "Muj klic";      // klic pro sifrovani
unsigned char iv[EVP_MAX_IV_LENGTH] = "inicial. vektor"; // inicializacni vektor
int otLength = 18;

void cryption(unsigned char in[], unsigned char out[], int inLength, int *outLen, EVP_CIPHER_CTX *ctx) {
    int tmpLength = 0;
    int res = EVP_CipherUpdate(ctx, out, &tmpLength, in, inLength);
    if (res != 1)
    {
        fprintf(stderr, "err update");
        exit(4);    
    }
    *outLen = tmpLength;
}

int byteToInt (const int* byteArray)
{
    return  (__uint32_t)byteArray[0] <<  0 |
            (__uint32_t)byteArray[1] <<  8 |
            (__uint32_t)byteArray[2] << 16 |
            (__uint32_t)byteArray[3] << 24;
}

void skipNBytes(int n, FILE *inputFile, FILE *outputFile) {
    for (int i = 0; i < n; i++) {
        char c = fgetc(inputFile);
        fputc(c, outputFile);
    }
}

int readNumber(FILE *inputFile, FILE *outputFile) {
    int arr[4];
    for (int i = 0; i < 4; i++) {
        int c = fgetc(inputFile);
        arr[i] = c;
        fputc(c, outputFile);
    }

    return byteToInt(arr);
}

void readData(int dataSize, FILE *inputFile, FILE *outputFile, const EVP_CIPHER *cipher, int type) {
    int res, textLength;
    int readDataNum = 0;
    int writtenDataNum = 0;
    int blockSize = EVP_CIPHER_block_size(cipher);
    char c;
    
    int dataSizeWithPadding = (type == dec) ? dataSize + blockSize : dataSize;


    EVP_CIPHER_CTX *ctx;
    ctx = EVP_CIPHER_CTX_new();
    if (ctx == NULL)
        exit(2);

    // Init cipher
    res = EVP_CipherInit_ex(ctx, cipher, NULL, key, iv, type);
    if (res != 1)
    {
        fprintf(stderr, "err init\n");
        exit(3);
    }

    // Input text
    unsigned char in[otLength];

    while (true) {
        int i = 0;
        for (; i < otLength && readDataNum < dataSizeWithPadding; i++, readDataNum++) {
            in[i] = fgetc(inputFile);
        }
        textLength = i;

        unsigned char out[textLength + blockSize - 1];

        int outLen;
        cryption(in, out, textLength, &outLen, ctx);

        for (int i = 0; i < outLen; i++, writtenDataNum++) {
            if (type == dec && writtenDataNum >= dataSize)
                break;
            fputc(out[i], outputFile);
        }

        // break while loop
        if (readDataNum >= dataSizeWithPadding ) {
            break;
        }
    }

    // Get type - 1 for encryption, 0 for decryption
    if (type == enc) {
        unsigned char outputText[blockSize + 1];
        res = EVP_CipherFinal_ex(ctx, outputText, &textLength);
        if (res != 1)
        {
            fprintf(stderr,"err final\n");
            exit(5);
        }
        for (int i = 0; i < textLength; i++, writtenDataNum++)
        {
            if (type == dec && writtenDataNum >= dataSize) 
                break;
            fputc(outputText[i], outputFile);
        }
    }


    EVP_CIPHER_CTX_free(ctx);
}

void checkHeader(FILE *inputFile, int fileSize, int dataSize, int dataStartPosition, int type) {
    fseek(inputFile, 0, SEEK_END);
    long size = ftell(inputFile); 

    fseek(inputFile, 0L, SEEK_END);
    long fileSizeFtell = ftell(inputFile);
    rewind(inputFile);
    fseek(inputFile, 0L, SEEK_SET);

    if (type == enc && fileSizeFtell != fileSize)
    {
        fprintf(stderr, "Error - file size\n");
        exit(1);
    }

    if (fileSize < dataStartPosition + 1)
    {
        fprintf(stderr, "Error - data start position\n");
        exit(1);
    }

    if (type == enc && dataStartPosition + dataSize != fileSize)
    {
        fprintf(stderr, "Error - data size\n");
        exit(1);
    }
}

void getCipherName(char arg[], char out[]) {
    if (strcmp(arg, "ecb") == 0) {
        strcpy(out, "DES-ECB");
    } else if (strcmp(arg, "cbc") == 0) {
        strcpy(out, "DES-CBC");
    } else {
        fprintf(stderr, "Error - cipher type\n");
        exit(1);
    }
}

int getType(char arg[]) {
    if (strcmp(arg, "-e") == 0) {
        return 1;
    } else if (strcmp(arg, "-d") == 0) {
        return 0;
    } else {
        fprintf(stderr, "Error - type of ..\n");
        exit(1);
    }
}

void getOutputFileName(int type, char cipherType[], char inputFileName[], char outputFileName[]) {
    strcpy(outputFileName, inputFileName);
    outputFileName[strlen(inputFileName) - 4] = 0;

    if (type == dec)
    {
        strcat(outputFileName, "_dec.bmp");
    }
    else if (type == enc)
    {
        if (strcmp(cipherType, "DES-ECB") == 0)
        {
            strcat(outputFileName, "_ecb.bmp");
        }
        else if (strcmp(cipherType, "DES-CBC") == 0)
        {
            strcat(outputFileName, "_cbc.bmp");
        }
    }
}

// gcc lab3_1.cpp -lcrypto; ./a.out -e ecb lab3-test-input/bad_homer-simpson4.bmp 
// ./a.out -e/-d ecb/cbc file
int main(int argc, char *argv[]) {
    // Check number of parameters
    if (argc != 4) {
        fprintf(stderr, "Error - number of parameters, example: ./a.out -e/-d ecb/cbc file\n");
        exit(1);
    }

    // Get type - 1 for encryption, 0 for decryption
    int type = getType(argv[1]);

    // Get cipher name - DES-ECB / DES-CBC
    char cipherName[9];
    getCipherName(argv[2], cipherName);

    // Get input file name
    char inputFileName[1024];
    strcpy(inputFileName, argv[3]);

    // Get ouput file name
    char outputFileName[1024];
    getOutputFileName(type, cipherName, inputFileName, outputFileName);

    // Init input file
    FILE *inputFile = fopen(inputFileName, "rb"); // read binary mode
    if (inputFile == NULL) {
        fprintf(stderr, "Error while opening the input file.\n");
        exit(1);
    }

    // Init output file
    FILE *outputFile = fopen(outputFileName, "wb"); // write binary mode
    if (outputFile == NULL) {
        fprintf(stderr, "Error while opening the output file.\n");
        exit(1);
    }

    // Init cipher
    const EVP_CIPHER *cipher;
    OpenSSL_add_all_ciphers();
    cipher = EVP_get_cipherbyname(cipherName);
    if (!cipher)
    {
        fprintf(stderr, "Cipher %s doesn't exist.\n", cipherName);
        exit(1);
    }

    // Read header
    skipNBytes(2, inputFile, outputFile);
    int fileSize = readNumber(inputFile, outputFile);
    skipNBytes(4, inputFile, outputFile);
    int dataStartPosition = readNumber(inputFile, outputFile);
    skipNBytes(dataStartPosition - 14, inputFile, outputFile);
    int dataSize = fileSize - dataStartPosition;

    checkHeader(inputFile, fileSize, dataSize, dataStartPosition, type);

    // Set position indicator associated with the stream to the beginning of the data
    fseek(inputFile, dataStartPosition, SEEK_SET); 

    // Read data
    readData(dataSize, inputFile, outputFile, cipher, type);

    // printf("dataSize:%d,fileSize:%d,dataStart:%d",dataSize, fileSize, dataStartPosition);

    fclose(inputFile);
    fclose(outputFile);

    return 0;
}

// gcc lab3_1.cpp -lcrypto; ./a.out -e ecb /Users/michalcerny/scientist.bmp
// gcc lab3_1.cpp -lcrypto; ./a.out -d ecb /Users/michalcerny/scientist_ecb.bmp

// gcc lab3_1.cpp -lcrypto; ./a.out -e cbc /Users/michalcerny/scientist.bmp 
// gcc lab3_1.cpp -lcrypto; ./a.out -d cbc /Users/michalcerny/scientist_cbc.bmp 


//
// Rozdíl cbc vs ecb
//
// Rozdíl je vidět na první pohled na zašifrované soubory - u ecb je obrázek stále rozpoznatelný, u cbc je vidět pouze šum.

// The Advanced Encryption Standard (AES), is a block cipher adopted as an encryption standard by the U.S. government for military and government use.

// ECB (Electronic Codebook) is essentially the first generation of the AES. It is the most basic form of block cipher encryption.

// CBC (Cipher Blocker Chaining) is an advanced form of block cipher encryption. With CBC mode encryption, each ciphertext block is dependent on all plaintext blocks processed up to that point. This adds an extra level of complexity to the encrypted data.
// Source: https://datalocker.com/what-is-the-difference-between-ecb-mode-versus-cbc-mode-aes-encryption/