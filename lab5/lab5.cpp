// cernym46
// g++ -lssl -lcrypto lab5.cpp; ./a.out out cert.cer
// openssl x509 -text -in cert.cer

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <unistd.h>
#include <iostream>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <openssl/evp.h>
#include <openssl/ssl.h>
#include <openssl/pem.h>

using namespace std;

#define HTTP_REQUEST "GET /student/odkazy HTTP/1.1\r\nConnection: close\r\nHost: fit.cvut.cz\r\n\r\n"
#define IP_ADDRESS "147.32.232.248" // nslookup fit.cvut.cz
#define BUFFER_SIZE 4096
#define PORT 443

// Zadání: stáhnout stránku předmětu https://fit.cvut.cz/student/odkazy do souboru;
void downloadToFile(char buffer[BUFFER_SIZE], SSL *ssl, char *outputFilePath) {
    snprintf(buffer, BUFFER_SIZE, HTTP_REQUEST);
    if (SSL_write(ssl, buffer, strlen(buffer) + 1) <= 0) {
        cout << "Error: SSL_write" << endl;
        exit(1);
    }
    int res;
    FILE *output_file = fopen(outputFilePath, "w");
    while ((res = SSL_read(ssl, buffer, BUFFER_SIZE)) > 0) {
        fwrite(buffer, sizeof(char), res, output_file);
    }
    fprintf(output_file, "\n");
    fclose(output_file);
    cout << "File was saved to " << outputFilePath << endl;
}


// Zadání: Uložit certifikát do souboru ve formátu PEM
void saveCertificateToFile(SSL *ssl, char *outputFilePath) {
    X509 *cert = SSL_get_peer_certificate(ssl);
    if (!cert) {
        cout << "Error: SSL_get_peer_certificate" << endl;
        exit(1);
    }
    FILE *cert_file = fopen(outputFilePath, "w");
    if (!PEM_write_X509(cert_file, cert)) {
        cout << "Error: PEM_write_X509" << endl;
        exit(1);
    }
    fclose(cert_file);
    cout << "Certificate was saved to " << outputFilePath << endl;

    
}

void printInfoAboutCertificate(char *outputFilePath) {
    string command = "openssl x509 -text -in " + string(outputFilePath);
    system(command.c_str());
}

void createConnection(int &sockfd) {
    struct sockaddr_in servaddr;

    sockfd=socket(AF_INET,SOCK_STREAM,0);
    
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr(IP_ADDRESS); //ip address
    servaddr.sin_port = htons(PORT); // port
    if (connect(sockfd, (struct sockaddr *) &servaddr, sizeof(servaddr)) != 0) {
        cout << "Error: connect" << endl;
        exit(1);
    }
}

int main(int argc, char *argv[]) {
    // Check number of arguments
    if (argc != 3) {
        cout << "Usage: " << argv[0] << " output_file output_cert_file" << endl;
        exit(1);
    }

    // Init
    SSL_library_init();
    char buffer[BUFFER_SIZE];

    int sockfd;
    createConnection(sockfd);

    // Create context
    SSL_CTX *ctx = SSL_CTX_new(SSLv23_client_method());
    if (!ctx) {
        cout << "Error: SSL_CTX_new" << endl;
        exit(1);
    }
    SSL_CTX_set_options(ctx, SSL_OP_NO_SSLv2 | SSL_OP_NO_SSLv3 | SSL_OP_NO_TLSv1);

    // Create SSL structure
    SSL *ssl = SSL_new(ctx);
    if (!ssl) {
        cout << "Error: SSL_new" << endl;
        exit(1);
    }
    if (!SSL_set_fd(ssl, sockfd)) {
        cout << "Error: SSL_set_fd" << endl;
        exit(1);
    }
    if (SSL_connect(ssl) <= 0) {
        cout << "Error: SSL_connect" << endl;
        exit(1);
    }

    downloadToFile(buffer, ssl, argv[1]);
    saveCertificateToFile(ssl, argv[2]);
    printInfoAboutCertificate(argv[2]);

    // Clean
    SSL_shutdown(ssl);
    close(sockfd);
    SSL_free(ssl);
    SSL_CTX_free(ctx);
    return 0;
}