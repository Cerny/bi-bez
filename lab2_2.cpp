#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <openssl/evp.h>

static int hexval(char c)
{
    char str[2] = {c, '\0'};
    return (int)strtol(str, NULL, 16);
}

static char hexdigit(int hexval)
{
    char buff[1];
    sprintf(buff, "%x", hexval);
    return buff[0];
}

static void xorHexStrings(const char *h1, const char *h2, char *out)
{
    int len1 = strlen(h1);
    int len2 = strlen(h2);
    int min = len1 < len2 ? len1 : len2;

    for(int i = 0; i < min; i++) // -1 because the last character is '\0'
    {
        char u1 = *h1++;
        char u2 = *h2++;
        *out++ = hexdigit(hexval(u1) ^ hexval(u2));
    }
    *out = '\0';
}

void stringToHexString(char *out, char *in)
{
    for (int i = 0; i < strlen(in); i++)
    {
        char buff[3];
        sprintf(buff, "%x", in[i]);
        buff[2] = '\0';
        out[2 * i] = buff[0];
        out[2 * i + 1] = buff[1];
        out[2 * i + 2] = '\0';
    }
}

void hexToString(char *out, char *in)
{
    for (int i = 0; i < strlen(in) + 3; i += 2)
    {
        char buff[3];
        // buff[2] = 0;
        buff[0] = in[i];
        buff[1] = in[i + 1];
        out[i / 2] = strtol(buff, NULL, 16);
    }
}

void alignInput(char *out, char *in) {
    if (strlen(in) % 2 == 1) {
        strcpy(out, "0");
        strcat(out, in);
    } else {
        strcpy(out, in);
    }
}

void decrypt(char *argv[])
{
    // Align input
    char st1[1024];
    alignInput(st1, argv[3]);
    char st2[1024];
    alignInput(st2, argv[4]);

    // Read input
    char otHex[1024];
    stringToHexString(otHex, argv[2]);

    // Get key stream
    char keyStream[1024];
    xorHexStrings(otHex, st2, keyStream);

    // Encrypt plain text with keyStream to hex
    char plainTextHex[1024];
    xorHexStrings(st1, keyStream, plainTextHex);

    // Convert plain text from hex to ascii string
    char plainText[1024];
    hexToString(plainText, plainTextHex);

    // Print decrypted text
    printf("%s\n", plainText);
}

void encrypt(unsigned char ot[1024])
{
    int res;
    unsigned char st[1024];                                       // sifrovany text
    unsigned char key[EVP_MAX_KEY_LENGTH] = "klic pro sifrovani"; // klic pro sifrovani
    unsigned char iv[EVP_MAX_IV_LENGTH] = "vektor";               // inicializacni vektor
    const char cipherName[] = "RC4";
    const EVP_CIPHER *cipher;

    OpenSSL_add_all_ciphers();
    cipher = EVP_get_cipherbyname(cipherName);
    if (!cipher)
    {
        printf("Sifra %s neexistuje.\n", cipherName);
        exit(1);
    }

    int otLength = strlen((const char *)ot);
    int stLength = 0;
    int tmpLength = 0;

    EVP_CIPHER_CTX *ctx; // context structure
    ctx = EVP_CIPHER_CTX_new();
    if (ctx == NULL)
        exit(2);

    /* Sifrovani */
    res = EVP_EncryptInit_ex(ctx, cipher, NULL, key, iv); // context init - set cipher, key, init vector
    if (res != 1)
        exit(3);
    res = EVP_EncryptUpdate(ctx, st, &tmpLength, ot, otLength); // encryption of pt
    if (res != 1)
        exit(4);
    stLength += tmpLength;
    res = EVP_EncryptFinal_ex(ctx, st + stLength, &tmpLength); // get the remaining ct
    if (res != 1)
        exit(5);
    stLength += tmpLength;

    /* Clean up */
    EVP_CIPHER_CTX_free(ctx);

    /* Vypsani zasifrovaneho textu. */
    printf("ST:");
    for (size_t i = 0; i < stLength; ++i)
    {
        printf("%02x", st[i]);
    }
    printf("\n");
}

// ./a.out ot1 st1 st2
// gcc lab2_2.cpp -lcrypto; ./a.out -d abcdefghijklmnopqrstuvwxyz0123 c6ba45ae10b3e27a31d704064fc96dde343a9dbf08f7c82bcc99a5c3dc5b f3b706b901b4f76678de001f4cd36bc0226897a408f39f3fd097e1979c1b
// gcc lab2_2.cpp -lcrypto; ./a.out -e
int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        printf("Err - number of parameters, example: -e/-d ot1 st1 st2\n");
        return 1;
    }

    if (strcmp(argv[1], "-d") == 0)
    {
        if (argc != 5)
        {
            printf("Err - number of parameters\n");
            return 1;
        }

        decrypt(argv);
    }
    else if (strcmp(argv[1], "-e") == 0)
    {
        unsigned char text1[1024] = "abcdefghijklmnopqrstuvwxyz";
        printf("OT:%s\n", text1);
        encrypt(text1);
        unsigned char text2[1024] = "Mohou byt ruzne delky retezcu-desifrujte jen smysluplnou cast";
        encrypt(text2);
    }

    exit(0);
}
