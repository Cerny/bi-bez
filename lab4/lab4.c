// cernym46 - Michal Cerny

#include <stdlib.h>
#include <openssl/evp.h>
#include <openssl/pem.h>
#include <openssl/rand.h>
#include <string.h>
#include <arpa/inet.h> /* For htonl() */

#define enc 1
#define dec 0
#define BUFFER_SIZE 1024

// Generate key (from courses)
// openssl genrsa -out privkey.pem 2048
// openssl rsa -in privkey.pem -pubout -out pubkey.pem

// Usage:
// ./a.out -e/-d key inputFile outputFile [cipherName]
// Cipher names: des-cbc, des-ecb, des-cfb, des-ofb, aes-128-cbc, aes-192-cbc, aes-256-cbc, aes-128-ecb, aes-192-ecb, aes-256-ecb, aes-128-gcm, aes-192-gcm, aes-256-gcm
// gcc -lcrypto lab4.c; ./a.out -e pubkey.pem scientist.bmp enc des-cbc
// gcc -lcrypto lab4.c; ./a.out -d privkey.pem enc dec.bmp

void decrypt(FILE *keyFile, FILE *outputFile, FILE *inputFile) {
    // Initialization of buffers
    unsigned char buffer[BUFFER_SIZE];
    unsigned char buffer_out[BUFFER_SIZE + EVP_MAX_IV_LENGTH];
    int len = 0;
    int len_out = 0;

    unsigned char iv[EVP_MAX_IV_LENGTH];
    EVP_CIPHER_CTX *ctx = EVP_CIPHER_CTX_new();
    uint32_t enc_key_size_n;
    int enc_key_size;

    EVP_PKEY *privkey = PEM_read_PrivateKey(keyFile, NULL, NULL, NULL);
    fclose(keyFile);

    // Initialization of decryption
    unsigned char *enc_key = (unsigned char *) malloc(EVP_PKEY_size(privkey));
    
    // Load parameters from file and load cipher

    // Get cipher name
    char cipher_name[BUFFER_SIZE];
    if (fscanf(inputFile, "%s\n", cipher_name) != 1) {
        fprintf(stderr, "Err: Cipher type\n");
        exit(1);
    }
    // Load cipher from its name
    const EVP_CIPHER *cipher = EVP_get_cipherbyname(cipher_name);
    if (!cipher) {
        fprintf(stderr, "Err: Unknown cipher type\n");
        exit(1);
    }

    // Get key size (32b)
    if (fread(&enc_key_size_n, sizeof enc_key_size_n, 1, inputFile) != 1) {
        fprintf(stderr, "Err: Loading key length\n");
        exit(1);
    }

    // ntohl converts the unsigned integer netlong from network byte order to host byte order.
    enc_key_size = ntohl(enc_key_size_n);
    if (enc_key_size > EVP_PKEY_size(privkey)) {
        fprintf(stderr, "Err: Key length\n");
        exit(1);
    }

    // Get key
    if (fread(enc_key, enc_key_size, 1, inputFile) != 1) {
        fprintf(stderr, "Err: Loading key\n");
        exit(1);
    }

    // Get iv
    if (fread(iv, EVP_MAX_IV_LENGTH, 1, inputFile) != 1) {
        fprintf(stderr, "Err: Loading iv\n");
        exit(1);
    }

    // Init cypher with enc_key, iv and privkey
    if (!EVP_OpenInit(ctx, cipher, enc_key, enc_key_size, iv, privkey)) {
        fprintf(stderr, "Err: OpenInit\n");
        exit(1);
    }

    // Decrypt and write all data to output file
    while ((len = fread(buffer, 1, sizeof buffer, inputFile)) > 0) {
        if (!EVP_OpenUpdate(ctx, buffer_out, &len_out, buffer, len)) {
            fprintf(stderr, "Err: OpenUpdate\n");
            exit(1);
        }
        if (fwrite(buffer_out, len_out, 1, outputFile) != 1) {
            fprintf(stderr, "Err: Writing to the output file\n");
            exit(1);
        }
    }

    // Load final data to the buffer
    if (!EVP_OpenFinal(ctx, buffer_out, &len_out)) {
        fprintf(stderr, "Err: OpenFinal\n");
        exit(1);
    }

    // Write final data to the ouput file
    if (fwrite(buffer_out, len_out, 1, outputFile) != 1) {
        fprintf(stderr, "Err: Writing last data\n");
        exit(1);
    }

    // Free memory
    EVP_PKEY_free(privkey);
    free(enc_key);

}

void encrypt(FILE *keyFile, FILE *outputFile, FILE *inputFile, char cipherName[1024]) {
    unsigned char buffer[BUFFER_SIZE];
    unsigned char buffer_out[BUFFER_SIZE + EVP_MAX_IV_LENGTH];
    int enc_key_size;
    int len_out;
    unsigned char iv[EVP_MAX_IV_LENGTH];
    EVP_CIPHER_CTX *ctx = EVP_CIPHER_CTX_new();
    EVP_PKEY *pubkey = PEM_read_PUBKEY(keyFile, NULL, NULL, NULL);
    fclose(keyFile);
    unsigned char *enc_key = (unsigned char *) malloc(EVP_PKEY_size(pubkey));
    int len = 0;

    const EVP_CIPHER *cipher = EVP_get_cipherbyname(cipherName);
    if (!cipher) {
        fprintf(stderr, "Err: Unknown cipher name\n");
        exit(1);
    }

    if (!EVP_SealInit(ctx, cipher, &enc_key, &enc_key_size, iv, &pubkey, 1)) {
        fprintf(stderr, "Err: SealInit error\n");
        exit(1);
    }
    
    uint32_t enc_key_size_n = htonl(enc_key_size);

    // Output file format:
    // cipherName\n
    // keySize(32b)|encodeKey|IV(EVP_MAX_IV_LENGTH)|data

    // Write cipher name to the output file
    fprintf(outputFile, "%s\n", cipherName);

    // Write key size to the ouput file (32b)
    if (fwrite(&enc_key_size_n, sizeof(enc_key_size_n), 1, outputFile) != 1) {
        fprintf(stderr, "Err: Writing size of key\n");
        exit(1);
    }

    // Write encode key to the ouput file
    if (fwrite(enc_key, enc_key_size, 1, outputFile) != 1) {
        fprintf(stderr, "Err: Writing key\n");
        exit(1);
    }

    // Write IV to the ouput file
    if (fwrite(iv, EVP_MAX_IV_LENGTH, 1, outputFile) != 1) {
        fprintf(stderr, "Err: Writing iv\n");
        exit(1);
    }

    // Read input file and encode 
    while ((len = fread(buffer, 1, sizeof buffer, inputFile)) > 0) {
        if (!EVP_SealUpdate(ctx, buffer_out, &len_out, buffer, len)) {
            fprintf(stderr, "Err: SealUpdate\n");
            exit(1);
        }
        if (fwrite(buffer_out, len_out, 1, outputFile) != 1) {
            fprintf(stderr, "Err: Writing to the output file\n");
            exit(1);
        }
    }

    // Load final data to the buffer
    if (!EVP_SealFinal(ctx, buffer_out, &len_out)) {
        fprintf(stderr, "Err: SealFinal\n");
        exit(1);
    }

    // Write final data to the ouput file
    if (fwrite(buffer_out, len_out, 1, outputFile) != 1) {
        fprintf(stderr, "Err: Writing last data\n");
        exit(1);
    }

    // Free memory
    EVP_PKEY_free(pubkey);
    free(enc_key);
}


int getType(char arg[]) {
    if (strcmp(arg, "-e") == 0) {
        return 1;
    } else if (strcmp(arg, "-d") == 0) {
        return 0;
    } else {
        fprintf(stderr, "Error - type of ..\n");
        exit(1);
    }
}

// ./a.out inputFile outputFile keyFile cipherName
// ./a.out -e/-d key inputFile outputFile cipherName
int main(int argc, char *argv[]) {
    // Check number of arguments
    if (argc <= 2) {
        fprintf(stderr, "Err: Number of parameters\n");
        exit(1);
    }

    // Get type - 1 for encryption, 0 for decryption
    int type = getType(argv[1]);

    if ((type == enc && argc != 6) || (type == dec && argc != 5)) {
        fprintf(stderr, "Err: Number of parameters\n");
        exit(1);
    }

    // Initialization of random generator and OpenSSL ciphers
    if (RAND_load_file("/dev/random", 32) != 32) {
        fprintf(stderr, "Err: Init random generator\n");
        exit(1);
    }
    OpenSSL_add_all_ciphers();

    // Initialization of arguments

    char keyFileName[1024];
    strcpy(keyFileName, argv[2]);

    char inputFileName[1024];
    strcpy(inputFileName, argv[3]);

    char outputFileName[1024];
    strcpy(outputFileName, argv[4]);

    // Load files, key and cipher
    FILE *inputFile = fopen(inputFileName, "r");
    if (!inputFile) {
        fprintf(stderr, "Err: Opening input file\n");
        exit(1);
    }

    FILE *outputFile = fopen(outputFileName, "w");
    if (!outputFile) {
        fprintf(stderr, "Err: Opening output file\n");
        exit(1);
    }

    FILE *keyFile = fopen(keyFileName, "r");
    if (!keyFile) {
        fprintf(stderr, "Err: Opening pubkey file\n");
        exit(1);
    }

    if (type == enc) {
        char cipherName[1024];
        strcpy(cipherName, argv[5]);
        encrypt(keyFile, outputFile, inputFile, cipherName);
    } else {
        decrypt(keyFile, outputFile, inputFile);
    }

    // Close loaded files
    fclose(inputFile);
    fclose(outputFile);    

    return 0;
}
